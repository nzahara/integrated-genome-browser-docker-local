#
# IGB Dockerfile
# Builds image used to build IGB installers
# via Bitbucket pipelines.
# 
# Requires JREs for Linux, Windows, MacOS
# we add these to the installer for convenience of
# users so that they don't have to separately install
# Java JRE that works best with IGB. 
# 
# Other relevant files include:
#
#  - install4J configuration files in distribution directory of
#    IGB project
#  - bitbucket pipelines YML configuration file in top level of
#    IGB project directory
#  - README.md in this repository
#
FROM maven:3.5.0
COPY settings.xml /root/.m2/settings.xml
RUN \
    apt-get update && \
    apt-get install -y nano && \
    apt-get install -y zip && \
    # Removing openjdk, as openjdk 1.8 lacks required JavaFX libraries
    apt-get purge -y openjdk-\* && \
    rm docker-java-home && \
    # Get JDK for Linux 
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/jdk-8u212-linux-x64.tar.gz && \
    # Install Oracle JDK
    mkdir /usr/lib/jvm && \
    cp jdk-8u212-linux-x64.tar.gz /usr/lib/jvm/ && \
    tar xvzf jdk-8u212-linux-x64.tar.gz -C /usr/lib/jvm/ && \
    ln -sf usr/lib/jvm/jdk1.8.0_212 docker-java-home && \
    update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_212/bin/java" 1 && \
    update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_212/bin/javac" 1 && \
    update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.8.0_212/bin/javaws" 1 && \
    # Install Install4J
    # Question: Is this strictly required since we are actually using install4J plug-in?
    # Yes. The plug-in is simply a kind of wrapper that runs the installed software below.
    wget https://download-keycdn.ej-technologies.com/install4j/install4j_linux_7_0_8.deb && \
    dpkg -i install4j_linux_7_0_8.deb && \
    # Get JRE bundles that will be packaged with IGB
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-x86-1.8.0_212.tar.gz && \
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-amd64-1.8.0_212.tar.gz && \
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/macosx-amd64-1.8.0_212.tar.gz && \
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/linux-amd64-1.8.0_212.tar.gz && \
    # Move jre tarballs to /opt/install4j7/jres
    cp *.tar.gz /opt/install4j7/jres/ && \
	rm install4j_linux_7_0_8.deb && \
	rm jdk-8u212-linux-x64.tar.gz && \
    rm *.tar.gz
CMD ["bash"]